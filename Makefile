build:
	sudo docker build -t aoki/glytoucan .

buildrev:
	sudo docker build -t aoki/glytoucan_20150715 .

buildnc:
	sudo docker build --no-cache -t aoki/glytoucan .

runprod:
	sudo docker run -d -h local.glytoucan -p 91:80 --link glyspace_bluetree:test.glytoucan.org --link prod.squid_bluetree:rdf.glytoucan.org -v /opt/prod.glytoucan/apacherun:/var/run/apache2 -v /opt/prod.glytoucan/apachelogs:/var/log/apache2 -v /etc/localtime:/etc/localtime --link prod.stanza_bluetree:stanza.glytoucan.org --name="prod.apache.glytoucan" aoki/glytoucan

run:
	sudo docker run -d -h local.glytoucan -p 90:80 --link glyspace_bluetree:test.glytoucan.org --link beta.squid_bluetree:rdf.glytoucan.org -v /opt/glytoucan/apacherun:/var/run/apache2 -v /opt/glytoucan/apachelogs:/var/log/apache2 -v /etc/localtime:/etc/localtime --link stanza_bluetree:stanza.glytoucan.org --name="apache.glytoucan" aoki/glytoucan

runtest:
	sudo docker run -d -h local.glytoucan -p 90:80 -v /opt/glytoucan/tmp:/var/www/glytoucan.org/app/tmp -v /mnt/jenkins/workspace/glytoucan:/tmp/glytoucan.org -v /opt/glytoucan/apachelogs:/var/log/apache2 -v /etc/localtime:/etc/localtime --link glyspacesquid_bluetree:test.glytoucan.org --link stanza_bluetree:stanza.glytoucan.org --name="apache.glytoucan" aoki/glytoucan_test

runbeta:
	sudo docker run -d -h local.glytoucan -p 91:80 --link glyspace_bluetree:test.glytoucan.org --link rdf.glytoucan-squid_bluetree:rdf.glytoucan.org -v /opt/beta.glytoucan/apachelogs:/var/log/apache2 -v /etc/localtime:/etc/localtime -v /opt/beta.glytoucan/apachelogs:/var/log/apache2 --link beta.stanza_bluetree:stanza.glytoucan.org --name="beta.apache.glytoucan" aoki/glytoucan

runbranch:
	sudo docker run -d -h local.glytoucan -p 92:80 -v /opt/glytoucanbranch/tmp:/var/www/glytoucan.org/app/tmp -v /mnt/jenkins/workspace/glytoucan-branch:/tmp/glytoucan.org -v /opt/glytoucanbranch/apachelogs:/var/log/apache2 --link glyspacesquid_bluetree:test.glytoucan.org --name="glytoucan_branch" aoki/glytoucan_dev

rundev:
	sudo docker run -d -h local.glytoucan -p 90:80 --link glyspace_bluetree:test.glytoucan.org --link beta.squid_bluetree:rdf.glytoucan.org -v /opt/glytoucan/apacherun:/var/run/apache2 -v /opt/glytoucan/apachelogs:/var/log/apache2 -v /etc/localtime:/etc/localtime --link beta.stanza_bluetree:stanza.glytoucan.org --name="apache.glytoucan" aoki/glytoucan

rundevstanza:
	sudo docker run -d -h local.glytoucan -p 83:80 -v /opt/glytoucan/tmp:/var/www/glytoucan.org/app/tmp -v ~/workspace/glytoucan:/tmp/glytoucan.org --name="apache.glytoucan" --link stanza_bluetree:stanza.glytoucan.org aoki/glytoucan_dev

rundevglyspace:
	sudo docker run -d -h local.glytoucan -p 83:80 -v /opt/glytoucan/tmp:/var/www/glytoucan.org/app/tmp -v ~/workspace/glytoucan:/tmp/glytoucan.org --link glyspacesquid_bluetree:test.glytoucan.org --name="apache.glytoucan" aoki/glytoucan_dev

bash:
	sudo docker run --rm -it -h local.glytoucan -v /opt/glytoucan/tmp:/var/www/glytoucan.org/app/tmp --link docker-virtuoso_bluetree:test.ts.glytoucan.org --link glyspacesquid_bluetree:glytoucan.org --link stanza_bluetree:stanza aoki/glytoucan /bin/bash

bashdev:
	sudo docker run --rm -it -h local.glytoucan -p 81:80 -v /opt/glytoucan/tmp:/tmp/glytoucan -v /home/aoki/workspace/glytoucan:/tmp/glytoucan.org aoki/glytoucan_dev /bin/bash

bashtest:
	sudo docker run --rm -it -h local.glytoucan -p 82:80 -v /opt/glytoucan/tmp:/var/www/glytoucan.org/app/tmp -v /mnt/jenkins/workspace/glytoucan:/tmp/glytoucan.org --link glyspacesquid_bluetree:test.glytoucan.org --link stanza_bluetree:stanza.glytoucan.org aoki/glytoucan_dev /bin/bash

ps:
	sudo docker ps

stop:
	sudo docker stop apache.glytoucan

rm:
	sudo docker rm apache.glytoucan

logs:
	sudo docker logs glytoucan

prodlogs:
	sudo docker logs --tail=10000 -f prod.apache.glytoucan

cleanbranch:
	sudo docker stop glytoucan_branch
	sudo docker rm glytoucan_branch

cleanprod:
	sudo docker stop prod.apache.glytoucan
	sudo docker rm prod.apache.glytoucan

cleanbeta:
	sudo docker stop beta.apache.glytoucan
	sudo docker rm beta.apache.glytoucan

ip:
	sudo docker inspect -f "{{ .NetworkSettings.IPAddress }}" glytoucan

restart:
	sudo docker restart glytoucan

inspect:
	sudo docker inspect aoki/glytoucan

rerun: stop rm rund

cleandev: builddev stop rm rundev

cleandevstanza: builddev stopdev rmdev rundevstanza

clean: build stop rm rund

# not tested
dump:
	sudo docker export glytoucan> glytoucan.glycoinfo.tar

# not tested
load:
	cat glytoucan.glycoinfo.tar | docker import - aoki/docker-glytoucan:glytoucan
	
.PHONY: build run test clean
