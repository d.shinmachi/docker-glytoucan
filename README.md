# Docker file for glytoucan environment

Clone this project to create an instance of the glytoucan.org website.  Local environments can also be setup, currently accessing the test.glytoucan.org web service.

## General Requirements

You'll need Docker(https://docs.docker.com/installation/#installation) and a version of Make(https://www.gnu.org/software/make/) will be helpful.

You will also need at least read-only access to the glytoucan source code available on bitbucket.  https://bitbucket.org/aokinobu/glytoucan

## Quick start from scratch
```
#!bash

git clone https://aokinobu@bitbucket.org/aokinobu/docker-glytoucan.git
```

first you will need to build the base image, run:


```
#!bash

make build
```


then build the dev environment

```
#!bash

make builddev
```

This will build the dev environment using docker.

Now if your source code of the glytoucan.org folder is not located in /home/aoki/workspace/glytoucan, then you will need to specify what to mount to Docker in the Makefile.

Please refer to the rundev command in the Makefile:

```
#!Make


rundev:
	sudo docker run -d -h local.glytoucan -p 80:80 -v /home/aoki/workspace/glytoucan:/tmp/glytoucan.org --name="glytoucan_dev" aoki/glytoucan_dev

```

replace the "/home/aoki/workspace/glytoucan" part with the glytoucan source folder.

then, to setup the environment, enter:

```
#!bash
make rundev
```

This will run an instance of glytoucan.org on port 80.

Then point your browser to http://localhost/

That's it!

If you have made changes and would like to see them reflected, you will need to clean the environment:

```
#!bash
make cleandev
```
### Details

The production(prd) environment: This is the main environment and Dockerfile where a specific version or tag will be checked out and run.  By only adjusting the version number in this Dockerfile, the production environment will be created.

The dev environment: By extending the prd base this recreates the prd environment, except the location of the source files will be a mounted location.  Every time the docker instance is (re)started, the changed files are processed so that environment variables are configured for dev.

The test environment: This extends the prd base and is used to automatically pull/overwrite the latest code base.

### Options

If you are already using port 80 on your localhost, you can change the port number by modifying the -p parameter in the rundev command:
```
#!bash

-p 81:80
```