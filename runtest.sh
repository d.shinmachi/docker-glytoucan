#!/bin/bash
source /etc/apache2/envvars

echo copying glytoucan.org to /var/www
cp -R /tmp/glytoucan.org /var/www/
echo configure hostname macro in init.js.template and copy to init.js 
cd /var/www/glytoucan.org && sed -e "s/\${hostname}/test.glytoucan.org/" app/webroot/js/init.js.template > app/webroot/js/init.js
echo configure hostname macro in WebServiceModel.php.template and copy to WebServiceModel.php
cd /var/www/glytoucan.org && sed -e "s/\${protocol}/http/" -e "s/\${hostname}/test.glytoucan.org/" -e "s/\${servicehostname}/test.glytoucan.org/" app/Model/WebServiceModel.php.template > app/Model/WebServiceModel.php
echo configure hostname macro in Preferences/index.ctp.template and copy 
cd /var/www/glytoucan.org && sed -e "s/\${hostname}/test.glytoucan.org/" app/View/Preferences/index.ctp.template > app/View/Preferences/index.ctp
echo configure Glyspace in Model/Glyspace and copy 
cd /var/www/glytoucan.org && sed -e "s/\${protocol}/http/" -e "s/\${hostname}/test.glytoucan.org/" app/Model/GlySpace.php.template > app/Model/GlySpace.php

echo set permissions
chown -R www-data:www-data /var/www/glytoucan.org
chmod -R g+w /var/www/glytoucan.org/app/tmp

echo remove localization files
rm /var/www/glytoucan.org/app/tmp/*.doc

echo start apache
exec /usr/sbin/apache2 -e info -D FOREGROUND
